#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "tdataroot.h"

template <class ValType>
class TStack :public TDataRoot<ValType>
{
private:
	int Hi;
public:
	TStack(int Size = DefMemSize) :TDataRoot(Size), Hi(-1) {};
	TStack(const TStack& St)
	{
		if (St.pMem != nullptr)
		{
			MemSize = St.MemSize;
			DataCount = St.DataCount;
			Hi = St.Hi;
			pMem = new ValType[MemSize];
			for (int i = 0; i < DataCount; ++i)
				pMem[i] = St.pMem[i];

			SetRetCode(DataOK);
		}
		else
			SetRetCode(DataNoMem);
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	void  Put(const ValType &Val)
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsFull())
		{
			//	void *p = nullptr;
			//	SetMem(p, MemSize + DefMemSize);
			//	pMem[++Hi] = Val;
			//	DataCount++;
			SetRetCode(DataFull); //DataOk
		}
		else
		{
			pMem[++Hi] = Val;
			DataCount++;
			SetRetCode(DataOK);
		}

	}

	ValType Get()
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			ValType res = pMem[Hi--];
			DataCount--;
			if ((Hi>DefMemSize - 1) && (Hi%DefMemSize == 0))
			{
				void *p = nullptr;
				SetMem(p, MemSize - DefMemSize);
			}
			SetRetCode(DataOK);
			return res;
		}
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	ValType GetHighElem()
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			SetRetCode(DataOK);
			return pMem[Hi];
		}
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	virtual void Print()
	{
		if (pMem != nullptr)
		{
			for (int i = 0; i < DataCount; ++i)
				std::cout << pMem[i] << ' ' << std::endl;
			SetRetCode(DataOK);
		}
		else
			throw SetRetCode(DataNoMem);
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
protected:
	int  IsValid()
	{
		int res = 0;
		if (pMem == nullptr)
			res++;
		if (MemSize < DataCount)
			res += 2;
		return res;
	}
};
#endif