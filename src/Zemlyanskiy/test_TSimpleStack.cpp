#include "TSimpleStack.h"
#include <iostream>
#include "gtest.h"

TEST(TSimpleStack, created_stack_is_empty)
//������ ��� ��������� ���� ������
{
	TSimpleStack<int> stk;

	EXPECT_TRUE(stk.IsEmpty());
}

TEST(TSimpleStack, created_stack_is_not_nullptr)
//������ ��� ��������� ���� ����� ���� ���� ������
{
	TSimpleStack<int> stk;

	EXPECT_NE(&stk, nullptr);
}

TEST(TSimpleStack, stack_in_which_was_inserted_an_element_is_not_empty)
//����, � ������� ������������ ������� ��������, �� ������
{
	TSimpleStack<int> stk;
	stk.Put(0);

	EXPECT_FALSE(stk.IsEmpty());
}

TEST(TSimpleStack, stack_from_which_was_got_an_element_is_not_full)
//����, �� �������� �������� �������, ��������
{
	TSimpleStack<int> stk;
	for (int i = 0; i < MemSize; ++i)
		stk.Put(0);
	stk.Get();

	EXPECT_FALSE(stk.IsFull());
}

TEST(TSimpleStack, stack_returns_last_put_element)
//���� ���������� ��������� ����������� �������
{
	TSimpleStack<int> stk;
	stk.Put(5);

	EXPECT_EQ(5, stk.Get());
}

TEST(TSimpleStack, cant_get_from_empty_stack)
//������ ������� ������� �� ������� �����
{
	TSimpleStack<int> stk;

	EXPECT_ANY_THROW(stk.Get());
}

TEST(TSimpleStack, cant_put_in_full_stack)
//������ �������� ������� � ������ ����
{
	TSimpleStack<int> stk;
	for (int i = 0; i < MemSize; ++i)
		stk.Put(0);

	EXPECT_ANY_THROW(stk.Put(0));
}

TEST(TSimpleStack, can_copy_stack)
//����� ����������� ����
{
	TSimpleStack<int> stk1;
	for (int i = 0; i < MemSize; ++i)
		stk1.Put(0);

	EXPECT_NO_THROW(TSimpleStack<int> stk2(stk1));
}

TEST(TSimpleStack, copied_stack_is_equil_to_source_one)
//������������� ���� ����� ���������
{
	TSimpleStack<int> stk1;
	for (int i = 0; i < MemSize; ++i)
		stk1.Put(0);
	TSimpleStack<int> stk2(stk1);
	bool result = true;
	for (int i = 0; i < MemSize; ++i)
	if (stk1.Get() != stk2.Get())
	{
		result = false;
		break;
	}

	EXPECT_TRUE(result);
}

TEST(TSimpleStack, copied_stack_has_its_own_memory)
//� �������������� ����� ���� �����
{
	TSimpleStack<int> stk1;
	for (int i = 0; i < MemSize; ++i)
		stk1.Put(0);
	TSimpleStack<int> stk2(stk1);

	EXPECT_NE(&stk1, &stk2);
}