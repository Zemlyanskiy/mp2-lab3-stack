#include "TStack.h"
#include <cstdio>
#include "gtest.h"

TEST(TStack, created_stack_is_empty)
{
	TStack<int> stk;

	EXPECT_TRUE(stk.IsEmpty());
}

TEST(TStack, created_stack_is_not_nullptr)
{
	TStack<int> stk;

	EXPECT_NE(&stk, nullptr);
}

TEST(TStack, stack_in_which_was_inserted_an_element_is_not_empty)
{
	TStack<int> stk;
	stk.Put(0);

	EXPECT_FALSE(stk.IsEmpty());
}

TEST(TStack, stack_from_which_was_got_an_element_is_not_full)
{
	TStack<int> stk;
	for (int i = 0; i < DefMemSize; ++i)
		stk.Put(0);
	stk.Get();

	EXPECT_FALSE(stk.IsFull());
}

TEST(TStack, last_put_element_is_high_of_stack)
{
	TStack<int> stk;
	stk.Put(0);

	EXPECT_EQ(stk.Get(), stk.GetHighElem());
}

TEST(TStack, stack_returns_last_put_element)
{
	TStack<int> stk;
	stk.Put(5);

	EXPECT_EQ(5, stk.Get());
}

TEST(TStack, cant_get_from_empty_stack)
{
	TStack<int> stk;

	EXPECT_ANY_THROW(stk.Get());
}

TEST(TStack, can_put_in_stack_with_DefMemSize_size)
{
	TStack<int> stk;
	for (int i = 0; i < DefMemSize; ++i)
		stk.Put(0);

	EXPECT_NO_THROW(stk.Put(0));
}

TEST(TStack, can_copy_stack)
{
	TStack<int> stk1;
	for (int i = 0; i < DefMemSize; ++i)
		stk1.Put(0);

	EXPECT_NO_THROW(TStack<int> stk2(stk1));
}

TEST(TStack, copied_stack_is_equil_to_source_one)
{
	TStack<int> stk1;
	for (int i = 0; i < DefMemSize; ++i)
		stk1.Put(i);
	TStack<int> stk2(stk1);
	bool result = true;
	for (int i = 0; i < DefMemSize; ++i)
	if (stk1.Get() != stk2.Get())
	{
		result = false;
		break;
	}

	EXPECT_TRUE(result);
}

TEST(TStack, copied_stack_has_its_own_memory)
{
	TStack<int> stk1;
	for (int i = 0; i < DefMemSize; ++i)
		stk1.Put(0);
	TStack<int> stk2(stk1);

	EXPECT_NE(&stk1, &stk2);
}