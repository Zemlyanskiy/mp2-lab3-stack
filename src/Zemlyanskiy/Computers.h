#include "TStack.h"
#include <iostream>
#include <string>

using namespace std;

class Computer 
{
private:
	int **table;
	string task, POSTtask;
	int len;
public:
	//constr
	Computer(string str);
	~Computer();
	//methods
	bool check_brackets();
	double Calculation();
	//private:
	int get_priorite(char Operation);
	string ConversionExpression();
};