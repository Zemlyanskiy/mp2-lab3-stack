#include "Computers.h"

Computer::Computer(string str)
{
	task = str;

	len = task.length();

	table = new int*[len / 2]; //������� ������.
	for (int i = 0; i < len / 2; i++) 
	{
		table[i] = new int[2];
		table[i][0] = 0;
		table[i][1] = 0;
	}
}
Computer::~Computer()
{
	delete[] table;
}

bool Computer::check_brackets() 
{

	TStack<int> brackets(len);
	int BRcount = 0, STRcount = 0;

	for (int i = 0; i < len; i++) 
	{
		if (task[i] == '(') 
		{
			brackets.Put(i);
			BRcount++;
		}
		if (task[i] == ')') 
		{
			if (brackets.IsEmpty())
				return false;
			table[STRcount][0] = brackets.Get();
			table[STRcount++][1] = i;
			BRcount++;
		}
		if (!brackets.IsEmpty() && i + 1 == len)
			return false;
	}
}
int Computer::get_priorite(char c)
{
	switch (c) 
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;

	}
	if ((c >= '0') && (c <= '9') || (c == '.'))
		return -1;
	else
		return -3;
};
string Computer::ConversionExpression()
{
	if (!check_brackets()) 
	{
		cout << "Error! Add/clean the bracket(s)" << endl;
		POSTtask = "error";
	}
	else
	{
		TStack<int> operation(256);
		for (int i = 0; i <= len; i++)
		{
			if (get_priorite(task[i]) == -1) 
			{
				POSTtask += task[i];
				continue;
			}
			if (get_priorite(task[i]) == 0)
				operation.Put(task[i]);
			if (get_priorite(task[i]) >= 2)
			{
				if ((operation.IsEmpty()) || (get_priorite(task[i]) > get_priorite((char)operation.GetHighElem())))
					operation.Put(task[i]);
				else
				{
					while (true)
					{
						POSTtask += (char)operation.Get();
						if (get_priorite(task[i]) > get_priorite((char)operation.GetHighElem()) || operation.IsEmpty())
							break;
					}
					operation.Put(task[i]);
				}
				POSTtask += ' ';
			}
			if (get_priorite(task[i]) == 1)
			{
				while (true)
				{
					if (operation.IsEmpty())
						break;
					if (get_priorite((char)operation.GetHighElem()) == 0)
					{
						operation.Get();
						break;
					}
					POSTtask += (char)operation.Get();
				}
			}
		}
		while (!operation.IsEmpty())
			POSTtask += (char)operation.Get();
	}
	//  cout << "The expression in Postfix form\n" << POSTtask << endl;
	return POSTtask;
}
double Computer::Calculation()
{
	POSTtask = ConversionExpression();
	TStack<double> operand(256);
	string number = "";
	int i = 0;
	double first = 0, second = 0, temp;
	while (i < POSTtask.length())
	{
		while ((i < POSTtask.length() && get_priorite(POSTtask[i]) == -1))
		//������ ����� �� ������ � �������� ������
		{
			number += POSTtask[i];
			i++;
		}
		number += '\0';
		if (number[0] != '\0')//������� �������� ������ � double
			operand.Put(stod(number));
		number = "";
		if (POSTtask[i] == ' ')
		{
			i++;
			continue;
		}
		if (get_priorite(POSTtask[i]) == 2 || get_priorite(POSTtask[i]) == 3)
		{
			second = operand.Get();
			first = operand.Get();
			switch (POSTtask[i])
			{
			case '+': operand.Put(first + second); break;
			case '-': operand.Put(first - second); break;
			case '*': operand.Put(first * second); break;
			case '/': operand.Put(first / second); break;
			}
		}
		i++;
	}
	return operand.Get();
}