// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem
#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  

#define DataEmpty - 101  
 #define DataFull - 102  
 #define DataNoMem - 103 

enum TMemType { MEM_HOLDER, MEM_RENTER };

template<class ValType>
class TDataRoot : public TDataCom
{
protected:
	ValType* pMem;   
	int MemSize;     
	int DataCount;   
	TMemType MemType; 

	void SetMem(void *p, int Size)
	{
		if (MemType == MEM_RENTER){
			MemSize = Size;
			for (int i = 0; i < DataCount; ++i)
				*((ValType*)(ValType*)p + i) = pMem[i];
			pMem = (ValType*)p;


			SetRetCode(DataOK);

		}
		if (MemType == MEM_HOLDER)
		{
			if (Size > 0)
			{
				ValType *pTempMem = pMem;
				pMem = new ValType[Size];
				for (int i = 0; i < DataCount; ++i)
					pMem[i] = *(pTempMem + i);
				MemSize = Size;
				delete[]pTempMem;

				SetRetCode(DataOK);
			}
		}
		else if (Size <= 0)
			SetRetCode(DataErr);
	}
public:
	TDataRoot(int Size = DefMemSize) :TDataCom()
	{
		DataCount = 0;
		if (Size == 0)
		{
			MemSize = 0;
			pMem = nullptr;
			MemType = MEM_RENTER;

			SetRetCode(DataOK);
		}
		else if (Size > 0)
		{
			MemSize = Size;
			pMem = new ValType[MemSize];
			MemType = MEM_HOLDER;

			SetRetCode(DataOK);
		}
		else
			SetRetCode(DataErr);
	}
	virtual ~TDataRoot()
	{
		if (MemType == MEM_HOLDER)
			delete pMem;
		pMem = nullptr;
	}

	virtual bool IsEmpty(void) const	
	{
		return DataCount == 0;
	}
	virtual bool IsFull(void) const		
	{
		return DataCount == MemSize;
	}
	virtual void  Put(const ValType &Val) = 0;	
	virtual ValType Get(void) = 0;			

	virtual int  IsValid() = 0;			
	virtual void Print() = 0;

	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

#endif
