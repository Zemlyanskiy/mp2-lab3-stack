# Методы программирования 2: Стек
## Введение
Лабораторная работа направлена на практическое освоение динамической структуры данных **Стек**. В качестве области приложений выбрана тема вычисления арифметических выражений, возникающей при трансляции программ на языке программирования высокого уровня в исполняемые программы.
## Цели и задачи
В рамках лабораторной работы ставится задача разработки двух видов стеков:
- простейшего, основанного на статическом массиве (класс TSimpleStack)
- более сложного, основанного на использовании динамической структуры (класс TStack).
С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. Сложность выражения ограничена только длиной строки.
В процессе выполнения лабораторной работы требуется использовать систему контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.
Выполнение работы предполагает решение следующих задач:
1. Разработка класса TSimpleStack на основе массива фиксированной длины.
2. Реализация методов класса TDataRoot согласно заданному интерфейсу.
3. Разработка класса TStack, являющегося производным классом от TDataRoot.
4. Разработка тестов для проверки работоспособности стеков.
5. Реализация алгоритма проверки правильности введенного арифметического выражения.
6. Реализация алгоритмов разбора и вычисления арифметического выражения.
7. Обеспечение работоспособности тестов и примера использования.
### Исходный код
#### Реализация шаблонного класса TSimpleStack на основе массива фиксированной длины
```c++
#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

//Error 1: "Stack is full."
//Error 2: "Stack is empty"

#define MemSize 25

template<class ValType>
class TSimpleStack
{
protected:
	ValType Mem[MemSize];
	int Top;
public:
	TSimpleStack() { Top = -1; }
	TSimpleStack(const ValType&St) :Top(St.Top)
	{
		for (int i = 0; i < Top; ++i)
			Mem[i] = St.Mem[i];
	}
	bool IsEmpty(void)const { return Top == -1; }
	bool IsFull(void)const { return Top == MemSize - 1; }
	void Put(const ValType Val)
	{
		if (IsFull())
			throw 1;
		else
			Mem[++Top] = Val;
	}
	ValType Get(void)
	{
		if (IsEmpty())
			throw 2;
		else
			return Mem[Top--];
	}
};
#endif
```
#### Реализация методов класса TDataRoot
Класс TDataRoot это дочерний абстрактный класс от класса TDataCom,реализованный для удобства на шаблонах.
Он содержит в себе практически все необходимые поля и реализацию всех необходимых виртуальных методов для обоих стеков. Очень удобно,что благодаря данному файлу мы имеем возможность не дублировать код в статическом и динамическом стеках.
```c++
#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  

#define DataEmpty - 101  
 #define DataFull - 102  
 #define DataNoMem - 103 

enum TMemType { MEM_HOLDER, MEM_RENTER };

template<class ValType>
class TDataRoot : public TDataCom
{
protected:
	ValType* pMem;   
	int MemSize;     
	int DataCount;   
	TMemType MemType; 

	void SetMem(void *p, int Size)
	{
		if (MemType == MEM_RENTER){
			MemSize = Size;
			for (int i = 0; i < DataCount; ++i)
				*((ValType*)(ValType*)p + i) = pMem[i];
			pMem = (ValType*)p;


			SetRetCode(DataOK);

		}
		if (MemType == MEM_HOLDER)
		{
			if (Size > 0)
			{
				ValType *pTempMem = pMem;
				pMem = new ValType[Size];
				for (int i = 0; i < DataCount; ++i)
					pMem[i] = *(pTempMem + i);
				MemSize = Size;
				delete[]pTempMem;

				SetRetCode(DataOK);
			}
		}
		else if (Size <= 0)
			SetRetCode(DataErr);
	}
public:
	TDataRoot(int Size = DefMemSize) :TDataCom()
	{
		DataCount = 0;
		if (Size == 0)
		{
			MemSize = 0;
			pMem = nullptr;
			MemType = MEM_RENTER;

			SetRetCode(DataOK);
		}
		else if (Size > 0)
		{
			MemSize = Size;
			pMem = new ValType[MemSize];
			MemType = MEM_HOLDER;

			SetRetCode(DataOK);
		}
		else
			SetRetCode(DataErr);
	}
	virtual ~TDataRoot()
	{
		if (MemType == MEM_HOLDER)
			delete pMem;
		pMem = nullptr;
	}

	virtual bool IsEmpty(void) const	
	{
		return DataCount == 0;
	}
	virtual bool IsFull(void) const		
	{
		return DataCount == MemSize;
	}
	virtual void  Put(const ValType &Val) = 0;	
	virtual ValType Get(void) = 0;			

	virtual int  IsValid() = 0;			
	virtual void Print() = 0;

	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

#endif
```
#### Код класса TDataCom
Устраревший по своей сути класс. Так как намного проще отлавливать исключения методами языка.
```c++
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

// TDataCom является общим базовым классом
class TDataCom
{
protected:
  int RetCode; // Код завершения

  int SetRetCode(int ret) { return RetCode = ret; }
public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}
  int GetRetCode()
  {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```
#### Реализация динамического стека TStack
Класс TStack является почти бесконечным стеком. Идея состоит в том, что многократно(пока техника позволяет) добавляется статический массив, тем самым отодвигая "потолок" стека. Сделан на шаблонах.Память добавляется и забирается при помощи метода setmem из dataroot.
```c++
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "tdataroot.h"

template <class ValType>
class TStack :public TDataRoot<ValType>
{
private:
	int Hi;
public:
	TStack(int Size = DefMemSize) :TDataRoot(Size), Hi(-1) {};
	TStack(const TStack& St)
	{
		if (St.pMem != nullptr)
		{
			MemSize = St.MemSize;
			DataCount = St.DataCount;
			Hi = St.Hi;
			pMem = new ValType[MemSize];
			for (int i = 0; i < DataCount; ++i)
				pMem[i] = St.pMem[i];

			SetRetCode(DataOK);
		}
		else
			SetRetCode(DataNoMem);
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	void  Put(const ValType &Val)
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsFull())
		{
			//	void *p = nullptr;
			//	SetMem(p, MemSize + DefMemSize);
			//	pMem[++Hi] = Val;
			//	DataCount++;
			SetRetCode(DataFull); //DataOk
		}
		else
		{
			pMem[++Hi] = Val;
			DataCount++;
			SetRetCode(DataOK);
		}

	}

	ValType Get()
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			ValType res = pMem[Hi--];
			DataCount--;
			if ((Hi>DefMemSize - 1) && (Hi%DefMemSize == 0))
			{
				void *p = nullptr;
				SetMem(p, MemSize - DefMemSize);
			}
			SetRetCode(DataOK);
			return res;
		}
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	ValType GetHighElem()
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			SetRetCode(DataOK);
			return pMem[Hi];
		}
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	virtual void Print()
	{
		if (pMem != nullptr)
		{
			for (int i = 0; i < DataCount; ++i)
				std::cout << pMem[i] << ' ' << std::endl;
			SetRetCode(DataOK);
		}
		else
			throw SetRetCode(DataNoMem);
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
protected:
	int  IsValid()
	{
		int res = 0;
		if (pMem == nullptr)
			res++;
		if (MemSize < DataCount)
			res += 2;
		return res;
	}
};
#endif
```
#### Обработка исключений стеков
Разработка тестов для проверки работоспособности стеков
В первую очередь тесты для классов TSimpleStack и TStack должны проверять очевидные заключения:
- Созданный стек является пустым;
- Стек, в который выполнена вставка, не пуст;
- Стек после операции исключения не полон;
- При выборке значения из стека извлекается последнее вставленное значение;
- Выборка значения из пустого стека выбрасывает исключение;
- Вставка значения в полный стек выбрасывает исключения (для TSimpleStack)
#### Исходный код тестов для статического стека TSimpleStack
```c++
#include "TSimpleStack.h"
#include <iostream>
#include "gtest.h"

TEST(TSimpleStack, created_stack_is_empty)
//только что созданный стек пустой
{
	TSimpleStack<int> stk;

	EXPECT_TRUE(stk.IsEmpty());
}

TEST(TSimpleStack, created_stack_is_not_nullptr)
//только что созданный стек имеет свой блок памяти
{
	TSimpleStack<int> stk;

	EXPECT_NE(&stk, nullptr);
}

TEST(TSimpleStack, stack_in_which_was_inserted_an_element_is_not_empty)
//стек, в который производится вставка элемента, не пустой
{
	TSimpleStack<int> stk;
	stk.Put(0);

	EXPECT_FALSE(stk.IsEmpty());
}

TEST(TSimpleStack, stack_from_which_was_got_an_element_is_not_full)
//стек, из которого извлекли элемент, неполный
{
	TSimpleStack<int> stk;
	for (int i = 0; i < MemSize; ++i)
		stk.Put(0);
	stk.Get();

	EXPECT_FALSE(stk.IsFull());
}

TEST(TSimpleStack, stack_returns_last_put_element)
//стек возвращает последний вставленный элемент
{
	TSimpleStack<int> stk;
	stk.Put(5);

	EXPECT_EQ(5, stk.Get());
}

TEST(TSimpleStack, cant_get_from_empty_stack)
//нельзя извлечь элемент из пустого стека
{
	TSimpleStack<int> stk;

	EXPECT_ANY_THROW(stk.Get());
}

TEST(TSimpleStack, cant_put_in_full_stack)
//нельзя вставить элемент в полный стек
{
	TSimpleStack<int> stk;
	for (int i = 0; i < MemSize; ++i)
		stk.Put(0);

	EXPECT_ANY_THROW(stk.Put(0));
}

TEST(TSimpleStack, can_copy_stack)
//можно скопировать стек
{
	TSimpleStack<int> stk1;
	for (int i = 0; i < MemSize; ++i)
		stk1.Put(0);

	EXPECT_NO_THROW(TSimpleStack<int> stk2(stk1));
}

TEST(TSimpleStack, copied_stack_is_equil_to_source_one)
//скопированный стек равен оригиналу
{
	TSimpleStack<int> stk1;
	for (int i = 0; i < MemSize; ++i)
		stk1.Put(0);
	TSimpleStack<int> stk2(stk1);
	bool result = true;
	for (int i = 0; i < MemSize; ++i)
	if (stk1.Get() != stk2.Get())
	{
		result = false;
		break;
	}

	EXPECT_TRUE(result);
}

TEST(TSimpleStack, copied_stack_has_its_own_memory)
//у скопированного стека свой адрес
{
	TSimpleStack<int> stk1;
	for (int i = 0; i < MemSize; ++i)
		stk1.Put(0);
	TSimpleStack<int> stk2(stk1);

	EXPECT_NE(&stk1, &stk2);
}
```
#### Исходный код тестов для динамического стека TStack
```c++
#include "TStack.h"
#include <cstdio>
#include "gtest.h"

TEST(TStack, created_stack_is_empty)
{
	TStack<int> stk;

	EXPECT_TRUE(stk.IsEmpty());
}

TEST(TStack, created_stack_is_not_nullptr)
{
	TStack<int> stk;

	EXPECT_NE(&stk, nullptr);
}

TEST(TStack, stack_in_which_was_inserted_an_element_is_not_empty)
{
	TStack<int> stk;
	stk.Put(0);

	EXPECT_FALSE(stk.IsEmpty());
}

TEST(TStack, stack_from_which_was_got_an_element_is_not_full)
{
	TStack<int> stk;
	for (int i = 0; i < DefMemSize; ++i)
		stk.Put(0);
	stk.Get();

	EXPECT_FALSE(stk.IsFull());
}

TEST(TStack, last_put_element_is_high_of_stack)
{
	TStack<int> stk;
	stk.Put(0);

	EXPECT_EQ(stk.Get(), stk.GetHighElem());
}

TEST(TStack, stack_returns_last_put_element)
{
	TStack<int> stk;
	stk.Put(5);

	EXPECT_EQ(5, stk.Get());
}

TEST(TStack, cant_get_from_empty_stack)
{
	TStack<int> stk;

	EXPECT_ANY_THROW(stk.Get());
}

TEST(TStack, can_put_in_stack_with_DefMemSize_size)
{
	TStack<int> stk;
	for (int i = 0; i < DefMemSize; ++i)
		stk.Put(0);

	EXPECT_NO_THROW(stk.Put(0));
}

TEST(TStack, can_copy_stack)
{
	TStack<int> stk1;
	for (int i = 0; i < DefMemSize; ++i)
		stk1.Put(0);

	EXPECT_NO_THROW(TStack<int> stk2(stk1));
}

TEST(TStack, copied_stack_is_equil_to_source_one)
{
	TStack<int> stk1;
	for (int i = 0; i < DefMemSize; ++i)
		stk1.Put(i);
	TStack<int> stk2(stk1);
	bool result = true;
	for (int i = 0; i < DefMemSize; ++i)
	if (stk1.Get() != stk2.Get())
	{
		result = false;
		break;
	}

	EXPECT_TRUE(result);
}

TEST(TStack, copied_stack_has_its_own_memory)
{
	TStack<int> stk1;
	for (int i = 0; i < DefMemSize; ++i)
		stk1.Put(0);
	TStack<int> stk2(stk1);

	EXPECT_NE(&stk1, &stk2);
}
```
#### Демонстрация работы тестов
![](https://pp.vk.me/c636923/v636923362/2e016/Wthf7M_SmJI.jpg)
![](https://pp.vk.me/c636923/v636923362/2e00e/B8MXpyn4W7k.jpg)
#### Демострация работы тестового приложения main.cpp
![](https://pp.vk.me/c636923/v636923362/2e01d/SZSijQV5LYU.jpg)
#### Понятие арифметического выражения
Арифметическое выражение - выражение, в котором операндами являются объекты, над которыми выполняются арифметические операции. Например,
![page2](https://pp.vk.me/c636923/v636923362/2df18/-P60UMdUsRc.jpg)
При такой форме записи (называемой инфиксной, где знаки операций стоят между операндами) порядок действий определяется расстановкой скобок и приоритетом операций. Постфиксная (или обратная польская) форма записи не содержит скобок, а знаки операций следуют после соответствующих операндов. Тогда для приведённого примера постфиксная форма будет иметь вид:
![page1](https://pp.vk.me/c636923/v636923362/2df11/GEHk5M2K4O8.jpg)
Обратная польская нотация была разработана австралийским ученым Чарльзом Хэмблином в середине 50-х годов прошлого столетия на основе польской нотации, которая была предложена в 1920 году польским математиком Яном Лукасевичем. Эта нотация лежит в основе организации вычислений для арифметических выражений. Известный ученый Эдсгер Дейкстра предложил алгоритм для перевода выражений из инфиксной в постфиксную форму. Данный алгоритм основан на использовании стека.
#### Контроль над расстановкой скобок
В рамках данной лабораторной работы предлагается ограничить контроль только правильной расстановкой скобок . Таким образом, требуется проанализировать соответствие открывающих и закрывающих круглых скобок во введённом арифметическом выражении. Программа должна напечатать таблицу соответствия скобок, причем в таблице должно быть указано, для каких скобок отсутствуют парные им, а также общее количество найденных ошибок. Для идентификации скобок могут быть использованы их порядковые номера в выражении. Например, для арифметического выражения
![Page3](https://pp.vk.me/c636923/v636923362/2df0a/YS0bK2cWyN4.jpg)
должна быть напечатана таблица вида:
![Page4](https://pp.vk.me/c636923/v636923362/2df1f/-fjJ4uaCIjw.jpg)
Прочерки в таблице обозначают отсутствие соответствующей скобки. При отсутствии обнаруженных ошибок программа должна выдать соответствующее сообщение.
#### Перевод из одной формы в другую
В рамках данного задания требуется разработать алгоритм и составить программу для перевода арифметического выражения из инфиксной формы записи в постфиксную. Так как при такой записи несколько операндов могут следовать подряд, то при выводе они разделяются пробелами. Как результат программа должна напечатать постфиксную форму выражения или выдать сообщение о невозможности построения такой формы в случае обнаружения ошибок при расстановке скобок.
#### Вычисление выражения в постфиксной форме
Для выполнения данного задания необходимо разработать алгоритм и составить программу для вычисления арифметического выражения. Программа должна напечатать результат вычисления выражения или выдать сообщение о наличии нечисловых операндов.
#### Условия и ограничения
При выполнении лабораторной работы могут быть использованы следующие основные допущения:
- Можно предполагать, что арифметические выражения состоят не более чем из 255 символов.
- В качестве допустимых арифметических операций можно рассматривать только символы + (сложение), - (вычитание), * (умножение), / (деление).
### Алгоритм решения
#### Проверка скобок
На вход алгоритма поступает строка символов, на выходе должна быть выдана таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем.
- Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
- Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.
- Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.
- Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.
- Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.
#### Перевод в постфиксную форму
Данный алгоритм основан на использовании стека. На вход алгоритма поступает строка символов, на выходе должна быть получена строка с постфиксной формой. Каждой операции и скобкам приписывается приоритет.
- ( - 0

- ) - 1

- +- - 2
- */ - 3
Предполагается, что входная строка содержит синтаксически правильное выражение.
Входная строка просматривается посимвольно слева направо до достижения конца строки. Операндами будем считать любую последовательность символов входной строки, не совпадающую со знаками определённых в таблице операций. Операнды по мере их появления переписываются в выходную строку. При появлении во входной строке операции, происходит вычисление приоритета данной операции. Знак данной операции помещается в стек, если:
- Приоритет операции равен 0 (это « ( » ),
- Приоритет операции строго больше приоритета операции, лежащей на вершине стека,
- Стек пуст.
В противном случае из стека извлекаются все знаки операций с приоритетом больше или равным приоритету текущей операции. Они переписываются в выходную строку, после чего знак текущей операции помещается в стек. Имеется особенность в обработке закрывающей скобки. Появление закрывающей скобки во входной строке приводит к выталкиванию и записи в выходную строку всех знаков операций до появления открывающей скобки. Открывающая скобка из стека выталкивается, но в выходную строку не записывается. Таким образом, ни открывающая, ни закрывающая скобки в выходную строку не попадают. После просмотра всей входной строки происходит последовательное извлечение всех элементов стека с одновременной записью знаков операций, извлекаемых из стека, в выходную строку.
#### Вычисление
Алгоритм вычисления арифметического выражения за один просмотр входной строки основан на использовании постфиксной формы записи выражения и работы со стеком
Выражение просматривается посимвольно слева направо. При обнаружении операнда производится перевод его в числовую форму и помещение в стек (если операнд не является числом, то вычисление прекращается с выдачей сообщения об ошибке.) При обнаружении знака операции происходит извлечение из стека двух значений, которые рассматриваются как операнд2 и операнд1 соответственно, и над ними производится обрабатываемая операция. Результат этой операции помещается в стек. По окончании просмотра всего выражения из стека извлекается окончательный результат.
### Реализация алгоритмов разбора и вычисления арифметического выражения
#### Задание основных методов
```c++
#include "TStack.h"
#include <iostream>
#include <string>

using namespace std;

class Computer 
{
private:
	int **table;
	string task, POSTtask;
	int len;
public:
	//constr
	Computer(string str);
	~Computer();
	//methods
	bool check_brackets();
	double Calculation();
	//private:
	int get_priorite(char Operation);
	string ConversionExpression();
};
```
#### Реализация методов
```c++
#include "Computers.h"

Computer::Computer(string str)
{
	task = str;

	len = task.length();

	table = new int*[len / 2]; 
	//таблица скобок.
	for (int i = 0; i < len / 2; i++) 
	{
		table[i] = new int[2];
		table[i][0] = 0;
		table[i][1] = 0;
	}
}
Computer::~Computer()
{
	delete[] table;
}

bool Computer::check_brackets() 
{

	TStack<int> brackets(len);
	int BRcount = 0, STRcount = 0;

	for (int i = 0; i < len; i++) 
	{
		if (task[i] == '(') 
		{
			brackets.Put(i);
			BRcount++;
		}
		if (task[i] == ')') 
		{
			if (brackets.IsEmpty())
				return false;
			table[STRcount][0] = brackets.Get();
			table[STRcount++][1] = i;
			BRcount++;
		}
		if (!brackets.IsEmpty() && i + 1 == len)
			return false;
	}
}
int Computer::get_priorite(char c)
{
	switch (c) 
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;

	}
	if ((c >= '0') && (c <= '9') || (c == '.'))
		return -1;
	else
		return -3;
};
string Computer::ConversionExpression()
{
	if (!check_brackets()) 
	{
		cout << "Error! Add/clean the bracket(s)" << endl;
		POSTtask = "error";
	}
	else
	{
		TStack<int> operation(256);
		for (int i = 0; i <= len; i++)
		{
			if (get_priorite(task[i]) == -1) 
			{
				POSTtask += task[i];
				continue;
			}
			if (get_priorite(task[i]) == 0)
				operation.Put(task[i]);
			if (get_priorite(task[i]) >= 2)
			{
				if ((operation.IsEmpty()) || (get_priorite(task[i]) > get_priorite((char)operation.GetHighElem())))
					operation.Put(task[i]);
				else
				{
					while (true)
					{
						POSTtask += (char)operation.Get();
						if (get_priorite(task[i]) > get_priorite((char)operation.GetHighElem()) || operation.IsEmpty())
							break;
					}
					operation.Put(task[i]);
				}
				POSTtask += ' ';
			}
			if (get_priorite(task[i]) == 1)
			{
				while (true)
				{
					if (operation.IsEmpty())
						break;
					if (get_priorite((char)operation.GetHighElem()) == 0)
					{
						operation.Get();
						break;
					}
					POSTtask += (char)operation.Get();
				}
			}
		}
		while (!operation.IsEmpty())
			POSTtask += (char)operation.Get();
	}
	//  cout << "The expression in Postfix form\n" << POSTtask << endl;
	return POSTtask;
}
double Computer::Calculation()
{
	POSTtask = ConversionExpression();
	TStack<double> operand(256);
	string number = "";
	int i = 0;
	double first = 0, second = 0, temp;
	while (i < POSTtask.length())
	{
		while ((i < POSTtask.length() && get_priorite(POSTtask[i]) == -1))
		//запись числа из строки в числовую строку
		{
			number += POSTtask[i];
			i++;
		}
		number += '\0';
		if (number[0] != '\0')//перевод числовой строки в double
			operand.Put(stod(number));
		number = "";
		if (POSTtask[i] == ' ')
		{
			i++;
			continue;
		}
		if (get_priorite(POSTtask[i]) == 2 || get_priorite(POSTtask[i]) == 3)
		{
			second = operand.Get();
			first = operand.Get();
			switch (POSTtask[i])
			{
			case '+': operand.Put(first + second); break;
			case '-': operand.Put(first - second); break;
			case '*': operand.Put(first * second); break;
			case '/': operand.Put(first / second); break;
			}
		}
		i++;
	}
	return operand.Get();
}
```
#### Тестовое приложение для проверки работы алгоритмов
```c++
#include<iostream>
#include "Computers.h"

using namespace std;

void main() 
{
	string str;
	cout << "enter your string text" << endl;
	cin >> str;
	Computer co(str);

	cout << "postfix form\t" << co.ConversionExpression()<< endl;
	cout << "your answer\t" << co.Calculation() << endl;

	system("pause");
}
```
#### Демонстрация работы тестового приложения mymain.cpp
![](https://pp.vk.me/c636923/v636923362/2dfee/AtVggxpqt6o.jpg)
### Выводы:
**Выполнив данную работу мы научились:**
- Реализации простейшего стека, основанного на статическом массиве (класс TSimpleStack)
- Также более сложного стека, основанного на использовании динамической структуры (класс TStack)
- Написанию приложения, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем
- Реализации алгоритма перевода арифмитического выражения из инфиксной формы в постфиксную форму